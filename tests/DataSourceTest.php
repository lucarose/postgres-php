<?php declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

final class DataSourceTest extends TestCase
{
    protected static $ds;
    protected static $testTable;
    

    public static function setUpBeforeClass(): void{
        self::$ds = new DataSource();
        self::$testTable = $_ENV['projecttable'];
    }

    public function testCanConstruct(): void
    {
        $ds = new DataSource();
        $this->assertNotNull($ds);
    }

    public function testCanGetAll(): void
    {
        $rows = self::$ds->getAll(self::$testTable);
        $this->assertNotNull($rows);
        $this->assertGreaterThan(0, $rows);
    }

    public function testCanGetByID(): void
    {
        $row1 = self::$ds->getByID(self::$testTable, 1);
        $this->assertNotNull($row1);
    }

    public function testInvalidID(): void 
    {
        $invalidRow = self::$ds->getByID(self::$testTable, -1);
        $this->assertEmpty($invalidRow);
    }

    public function testGetAllInvalidTable(): void
    {
        $table_name = "38924l38945l2p";
        $rows = self::$ds->getAll($table_name);
        $this->assertNull($rows);
    }

    public function testGetIDInvalidTable(): void
    {
        $table_name = "38924l38945l2p";
        $rows = self::$ds->getByID($table_name,1);
        $this->assertNull($rows);
    }
}