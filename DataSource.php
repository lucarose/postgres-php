<?php

require_once __DIR__ . "/vendor/autoload.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

class DataSource {
    public function __construct() {
        $host = $_ENV["dbhost"];
        $dbname = $_ENV["dbname"];
        $user = $_ENV["dbuser"];
        $password = $_ENV["dbpass"];

        $dbconn = pg_connect("host=$host dbname=$dbname user=$user password=$password")
    or die("Could not connect: " . pg_last_error());
    }

    public function getAll(string $tableName) {
        $query = "SELECT * FROM $tableName";
        try {
            $result = pg_query($query);
         } catch (Exception $e) {
            //  in real life use case, would integrate with logging system
            echo $e->getMessage();
            return null;
         }

        $returnArr = array(array());
        for ($row = 0; $row < pg_numrows($result); $row++) {
            $arr = pg_fetch_array($result, $row, PGSQL_NUM);
            array_push($returnArr, $arr);
        }

        return $returnArr;
    }

    public function getByID(string $tableName, int $id) {
        $query = "SELECT * FROM $tableName WHERE id=$id";
        try {
            $result = pg_query($query);
         } catch (Exception $e) {
            echo $e->getMessage();
            return null;
         }

        $returnArr = pg_fetch_array($result, NULL, PGSQL_NUM);

        return $returnArr;
    }
}