DROP TABLE IF EXISTS tasks, projects, users;

CREATE TABLE users (
    id serial PRIMARY KEY,
    first_name varchar(50) NOT NULL,
    email varchar(100) NOT NULL
);


CREATE TABLE projects (
    id serial PRIMARY KEY,
    title varchar(50) NOT NULL,
    description varchar(2000),
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id)
);


CREATE TABLE tasks (
    id serial PRIMARY KEY,
    title varchar(50) NOT NULL,
    description varchar(2000),
    project_id integer,
    FOREIGN KEY (project_id) REFERENCES projects(id)
);

-- seed w/ sample values 

-- users

INSERT INTO users(first_name, email)
VALUES ('John', 'john@sample.edu');

INSERT INTO users(first_name, email)
VALUES ('Anna', 'anna@sample.edu');

INSERT INTO users(first_name, email)
VALUES ('Belle', 'belle@sample.edu');


-- projects

INSERT INTO projects(title, description, user_id)
VALUES ('First Mow', 'First time mowing the lawn this year', 1);

INSERT INTO projects(title, description, user_id)
VALUES ('Birthday Grouting', 'Grouting for my birthday', 1);

INSERT INTO projects(title, description, user_id)
VALUES ('Kitchen Remodel', 'Floral tile + countertop kitchen remodel', 2);

INSERT INTO projects(title, description, user_id)
VALUES ('Cat Race', 'Getting the local stray cats to race', 3);


-- tasks

INSERT INTO tasks(title, description, project_id)
VALUES ('Acquire Lawnmower', 'I think they have them at grocery stores sometimes', 1);

INSERT INTO tasks(title, description, project_id)
VALUES ('Acquire Grout', 'I think my friend had extra grout buckets...', 2);

INSERT INTO tasks(title, description, project_id)
VALUES ('Send Birthday Invites', 'To: Anna, Belle', 2);

INSERT INTO tasks(title, description, project_id)
VALUES ('Redo Tile', 'Finish putting up new floral tiles in kitchen', 3);

INSERT INTO tasks(title, description, project_id)
VALUES ('Refinish Countertops', 'Remove old countertops, replace with new quartz countertops', 3);

INSERT INTO tasks(title, description, project_id)
VALUES ('Acquire Local Cats', 'Tuna may work', 4);

INSERT INTO tasks(title, description, project_id)
VALUES ('Send Cat Race Invites', 'To Anna only. Do not invite John, he will make me grout.', 4);